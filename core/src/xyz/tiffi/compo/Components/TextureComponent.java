package xyz.tiffi.compo.Components;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import xyz.tiffi.compo.screens.GameScreen;

public class TextureComponent implements DrawAbleComponent {

	private Texture texture;
	public int priority;
	private Entity entity;
	private float scale;
	private float scaleX = 1;
	private float rotation;
	private boolean spinning, spinDown;
	GameScreen game;

	public TextureComponent() {
		// TODO Auto-generated constructor stub
	}

	public TextureComponent(String path, int priority, Entity entity) {
		this(path, priority, entity, 1);
	}

	public TextureComponent(String path, int priority, Entity entity, float scale) {
		this(path, priority, entity, scale, 0);
	}

	public TextureComponent(String path, int priority, Entity entity, float scale, GameScreen game) {

		this(path, priority, entity, scale, 0);
		this.game = game;
	}

	public TextureComponent(String path, int priority, Entity entity, float scale, float rotation) {
		this(path, priority, entity, scale, rotation, false);
	}

	public TextureComponent(String path, int priority, Entity entity, float scale, float rotation, boolean spinning) {
		this.texture = new Texture(path);
		this.priority = priority;
		this.entity = entity;
		this.scale = scale;
		this.rotation = rotation;
		this.spinning = spinning;
	}

	public void draw(SpriteBatch batch) {
		float x, y;
		BodyComponent bc = ComponentMapper.getFor(BodyComponent.class).get(entity);
		if (bc == null) {
			TransformComponent tc = ComponentMapper.getFor(TransformComponent.class).get(entity);
			x = tc.posX;
			y = tc.posY;
		} else {
			x = bc.getBody().getPosition().x;
			y = bc.getBody().getPosition().y;
		}
		if (spinning) {
			if (scaleX >= 1)
				spinDown = true;
			if (scaleX <= -1)
				spinDown = false;
			if (spinDown)
				scaleX -= .05f;
			else
				scaleX += .05f;
		}
		if (game == null)
			batch.draw(texture, x - (texture.getWidth() * scale) / 2f, y - (texture.getHeight() * scale) / 2f,
					(texture.getWidth() * scale) / 2f, (texture.getHeight() * scale) / 2f, texture.getWidth() * scale,
					texture.getHeight() * scale, scaleX, 1, rotation, 0, 0, texture.getWidth(), texture.getHeight(),
					false, false);
		else
			batch.draw(texture, x - (texture.getWidth() * scale) / 2f, y - (texture.getHeight() * scale) / 2f,
					((texture.getWidth() * scale) / 2f) , (texture.getHeight() * scale) / 2f, texture.getWidth() * scale,
					texture.getHeight() * scale, scaleX, 1, rotation, 0, 0, texture.getWidth(), texture.getHeight(),
					false, false);

	}

	public Texture getTexture() {
		return texture;
	}

	public void setTexture(Texture texture) {
		this.texture = texture;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public float getScale() {
		return scale;
	}

	public void setScale(float scale) {
		this.scale = scale;
	}

	@Override
	public int compareTo(DrawAbleComponent o) {
		return Integer.compare(priority, o.getPriority());

	}
}