package xyz.tiffi.compo.screens;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.Screen;

import xyz.tiffi.compo.Components.TextureComponent;
import xyz.tiffi.compo.Components.TransformComponent;
import xyz.tiffi.compo.GameCollision;
import xyz.tiffi.compo.GameConstants;
import xyz.tiffi.compo.Input;
import xyz.tiffi.compo.Main;
import xyz.tiffi.compo.Resources;
import xyz.tiffi.compo.Components.DrawAbleComponent;
import xyz.tiffi.compo.Components.ParticleComponent;
import xyz.tiffi.compo.systems.DestroyBodys;
import xyz.tiffi.compo.systems.EnemySystem;
import xyz.tiffi.compo.systems.MovementSystem;
import xyz.tiffi.compo.systems.PlayerSystem;
import xyz.tiffi.compo.systems.StarSystem;

public class GameScreen implements Screen {
	private SpriteBatch batch;
	private Camera camera;
	public int fingers;
	private BitmapFont font;
	private Input input;
	private Engine engine;
	private Family drawables;
	public World world;
	private Box2DDebugRenderer debugRenderer;
	public int score;
	public float starTime;
	public String playerName = "CHEF";
	public Main main;
	public int level = 1;

	public GameScreen(String playerName, Main main) {
		this.playerName = playerName;
		this.main = main;
	}

	@Override
	public void show() {
		Gdx.input.setOnscreenKeyboardVisible(false);
		System.out.println(playerName);
		world = new World(new Vector2(0, 0), false);
		debugRenderer = new Box2DDebugRenderer();
		batch = new SpriteBatch();
		engine = new Engine();
		ExtendViewport v = new ExtendViewport(GameConstants.SCREENWIDTH, GameConstants.SCREENHEIGHT);
		v.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		camera = v.getCamera();
		camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
		camera.update();
		font = new BitmapFont(Gdx.files.getFileHandle(Resources.FONT, FileType.Internal));
		font.getData().setScale(.3f, .3f);
		font.setColor(Color.RED);
		input = new Input();
		Gdx.input.setInputProcessor(input);
		engine.addSystem(new StarSystem(camera, this));
		engine.addSystem(new MovementSystem(world));
		engine.addSystem(new PlayerSystem(camera, world, v, this));
		engine.addSystem(new EnemySystem(this, world, camera));
		DestroyBodys destroyer = new DestroyBodys(world);
		engine.addSystem(destroyer);
		world.setContactListener(new GameCollision(destroyer, this, world));

		System.out.println("Gdx Width: " + Gdx.graphics.getWidth() + " Gdx Height: " + Gdx.graphics.getHeight()
				+ " SCREENWIDHT " + GameConstants.SCREENWIDTH + " SCREENHEIGHT: " + GameConstants.SCREENHEIGHT);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		camera.update();
		batch.setProjectionMatrix(camera.combined);
		engine.update(Gdx.graphics.getDeltaTime());

		batch.begin();
		for (DrawAbleComponent tc : getDrawables()) {
			tc.draw(batch);
		}
		font.draw(batch, "Level: " + level + " Score:" + score, camera.viewportWidth - 65, camera.viewportHeight - 3);
		batch.end();

//		debugRenderer.render(world, camera.combined);
		starTime += Gdx.graphics.getDeltaTime();
		if (starTime <= 1) {
			world.step(1 / 30f, 6, 2);
		} else
			world.step(1 / ((float) Gdx.graphics.getFramesPerSecond()), 6, 2);

		input.update();
		
//		if(Gdx.input.isKeyPressed(Keys.A))
//			score += 100;
		level = (int) Math.sqrt(score/(100f)) +1;
	}

	private List<DrawAbleComponent> getDrawables() {
		drawables = Family.all(TransformComponent.class).one(TextureComponent.class, ParticleComponent.class).get();
		ImmutableArray<Entity> drawAblesArray = engine.getEntitiesFor(drawables);
		List<DrawAbleComponent> drawablesList = new ArrayList<DrawAbleComponent>();
		for (Entity entity : drawAblesArray) {
			if (ComponentMapper.getFor(TextureComponent.class).get(entity) != null)
				drawablesList.add(ComponentMapper.getFor(TextureComponent.class).get(entity));
			else
				drawablesList.add(ComponentMapper.getFor(ParticleComponent.class).get(entity));

		}

		Collections.sort(drawablesList);
		return drawablesList;
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}
	
}
