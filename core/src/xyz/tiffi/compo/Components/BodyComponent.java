package xyz.tiffi.compo.Components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

import xyz.tiffi.compo.ShapeType;
import xyz.tiffi.compo.UserData;

import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;

public class BodyComponent implements Component {

	private World world;
	private Body body;

	public BodyComponent(World world, BodyType bodyType, float x, float y, Shape shape, float density, float friction,
			float resititution, Vector2 startForce, UserData userData) {
		this.world = world;
		BodyDef def = new BodyDef();
		def.type = bodyType;
		def.position.set(x, y);

		System.out.println(world);
		body = world.createBody(def);

		body.setUserData(userData);

		FixtureDef fixDef = new FixtureDef();
		fixDef.shape = shape;
		fixDef.density = density;
		fixDef.friction = friction;
		fixDef.restitution = resititution;
		fixDef.isSensor = true;

		body.createFixture(fixDef);

		shape.dispose();
		
		body.setLinearVelocity(startForce);

	}

	
	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

}
