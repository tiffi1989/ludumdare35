package xyz.tiffi.compo;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;

import xyz.tiffi.compo.systems.PlayerSystem;

public class Input implements InputProcessor {

	public int counter = 0;
	public Entity player;
	public Engine engine;
	public PlayerSystem playerSystem;
	public int controlFinger = -1;
	public float pushed;
	public boolean pushable = true;;

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		if (screenX < 200 && controlFinger == -1)
			controlFinger = pointer;
		if (screenX > 200) {
			addCounter();
		}

		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		if (pointer == controlFinger)
			controlFinger = -1;
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	public void setPlayer(Entity player) {
		this.player = player;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
		this.playerSystem = engine.getSystem(PlayerSystem.class);
	}

	public void update() {

		if (!pushable)
			pushed += Gdx.graphics.getDeltaTime();

		if (pushed > .2f) {
			if (player == null)
				return;
			switch (counter) {
			case 0:
				break;
			case 1:
				playerSystem.shoot(Resources.CIRCLE, ShapeType.CIRCLE);

				break;
			case 2:
				playerSystem.shoot(Resources.SQUARE, ShapeType.SQUARE);

				break;
			case 3:
				playerSystem.shoot(Resources.TRIFORCE, ShapeType.TRIANGLE);

				break;

			default:
				playerSystem.shoot(Resources.TRIFORCE, ShapeType.TRIANGLE);

				break;
			}

			counter = 0;
			pushable = true;
			pushed = 0;
		}

	}

	public void addCounter() {
		if (pushable == true)
			pushable = false;

		counter++;

	}
}