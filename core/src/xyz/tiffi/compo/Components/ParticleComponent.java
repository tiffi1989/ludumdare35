package xyz.tiffi.compo.Components;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import xyz.tiffi.compo.GameConstants;
import xyz.tiffi.compo.Resources;

public class ParticleComponent implements DrawAbleComponent, Comparable<DrawAbleComponent> {

	private ParticleEffect effect;
	private int priority;
	private Engine engine;
	private Entity entity;

	public ParticleComponent(String path, int priority, Entity entity, Engine engine) {
		this.priority = priority;
		this.engine = engine;
		this.entity = entity;
		float x, y;
		effect = new ParticleEffect();
		effect.load(Gdx.files.internal(Resources.EXPLOSION), Gdx.files.internal(""));
		effect.scaleEffect(GameConstants.PARTICLESCALE);

		TransformComponent tc = ComponentMapper.getFor(TransformComponent.class).get(entity);
		x = tc.posX;
		y = tc.posY;

		effect.setPosition(x, y);
		effect.start();
	}

	@Override
	public void draw(SpriteBatch batch) {
		effect.draw(batch, Gdx.graphics.getDeltaTime());
		
		if(effect.isComplete())
			engine.removeEntity(entity);
	}

	@Override
	public int compareTo(DrawAbleComponent o) {
		return Integer.compare(priority, o.getPriority());

	}

	@Override
	public int getPriority() {
		return priority;
	}

}
