package xyz.tiffi.compo.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.physics.box2d.World;

import xyz.tiffi.compo.GameConstants;
import xyz.tiffi.compo.Components.BodyComponent;
import xyz.tiffi.compo.Components.DTWOOSComponent;
import xyz.tiffi.compo.Components.TransformComponent;

public class MovementSystem extends EntitySystem {

	Family moveAbles = Family.all(TransformComponent.class).get();
	Family destroyAbles = Family.all(DTWOOSComponent.class).get();
	World world;

	public MovementSystem(World world) {
		this.world = world;
	}

	@Override
	public void update(float deltaTime) {
		ImmutableArray<Entity> moveArray = getEngine().getEntitiesFor(moveAbles);
		for (Entity entity : moveArray) {
			TransformComponent tc = ComponentMapper.getFor(TransformComponent.class).get(entity);
			tc.posX += tc.velX * deltaTime;
			tc.posY += tc.velY * deltaTime;
		}

		ImmutableArray<Entity> destroyArray = getEngine().getEntitiesFor(destroyAbles);
		for (Entity entity : moveArray) {
			float x, y;
			BodyComponent bc = ComponentMapper.getFor(BodyComponent.class).get(entity);
			if (bc == null) {
				TransformComponent tc = ComponentMapper.getFor(TransformComponent.class).get(entity);
				x = tc.posX;
				y = tc.posY;
			} else {
				x = bc.getBody().getPosition().x;
				y = bc.getBody().getPosition().y;
			}
			if (x >= GameConstants.SCREENWIDTH + 20 || x <= -20) {
				getEngine().removeEntity(entity);
				if (bc != null) {
					world.destroyBody(bc.getBody());

				}
			}

		}

	}

}
