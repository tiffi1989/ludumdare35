package xyz.tiffi.compo;

public class TextureLayers {
	
	public static final int UI = 1500;
	public static final int PLAYER = 1000;
	public static final int SHOTS = 500;
	public static final int ENEMYS = 100;
	public static final int STARS = 0;
	public static final int BACKGROUND = -1000;

}
