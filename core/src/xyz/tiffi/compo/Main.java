package xyz.tiffi.compo;

import com.badlogic.gdx.Game;

import xyz.tiffi.compo.screens.GameOverScreen;
import xyz.tiffi.compo.screens.GameScreen;
import xyz.tiffi.compo.screens.MainMenu;

public class Main extends Game {

	@Override
	public void create() {

//		this.setScreen(new GameScreen("Depp", this));
//		this.setScreen(new GameOverScreen(this, "Depp"));
		this.setScreen(new MainMenu(this));
	}

}
