package xyz.tiffi.compo.screens;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

import xyz.tiffi.compo.CustomLabel;
import xyz.tiffi.compo.GameConstants;
import xyz.tiffi.compo.Highscores;
import xyz.tiffi.compo.Main;
import xyz.tiffi.compo.Resources;
import xyz.tiffi.compo.Score;
import xyz.tiffi.compo.Components.DrawAbleComponent;
import xyz.tiffi.compo.Components.ParticleComponent;
import xyz.tiffi.compo.Components.TextureComponent;
import xyz.tiffi.compo.Components.TransformComponent;
import xyz.tiffi.compo.systems.MovementSystem;
import xyz.tiffi.compo.systems.StarSystem;

public class GameOverScreen implements Screen {

	Main main;

	SpriteBatch batch;
	Stage stage;
	BitmapFont font;
	Engine engine;
	private Family drawables;
	String labelText ="Please enter Your Name";
	CustomLabel label;
	String name;

	public GameOverScreen(final Main main, final String name, int playerScore) {
		this.name = name;
		this.main = main;
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);
		batch = new SpriteBatch();

		font = new BitmapFont(Gdx.files.getFileHandle(Resources.FONT, FileType.Internal));
		font.getData().setScale(.3f, .3f);
		font.setColor(Color.RED);

		LabelStyle lstyle = new LabelStyle(font, Color.RED);
		label = new CustomLabel("YOU LOST", lstyle);
		label.setPosition(5, 78);
		stage.addActor(label);
		Label label2 = new CustomLabel("HIGHSCORES:", lstyle);
		label2.setPosition(5, 70);
		stage.addActor(label2);
		Highscores high = new Highscores();
		high.add(new Score(name, playerScore));
		List<Score> highscores = high.load();
		System.out.println(highscores.size());
		Label label3 = new CustomLabel("# Name:", lstyle);
		label3.setPosition(50, GameConstants.SCREENHEIGHT-10);
		stage.addActor(label3);
		Label label4 = new CustomLabel("Score:", lstyle);
		label4.setPosition(120, GameConstants.SCREENHEIGHT-10);
		stage.addActor(label4);
		int counter = 1;
		for (Score score : highscores) {
			Label label = new CustomLabel(counter + ". " + score.getName().toLowerCase(), lstyle);
			label.setPosition(50, GameConstants.SCREENHEIGHT-10 - 7*counter);
			stage.addActor(label);
			Label scorel = new CustomLabel(score.getScore()+"", lstyle);
			scorel.setPosition(120, GameConstants.SCREENHEIGHT-10 - 7*counter);
			stage.addActor(scorel);
			counter++;
			if(counter > 10)
				break;
		}
		
		ExtendViewport v = new ExtendViewport(GameConstants.SCREENWIDTH, GameConstants.SCREENHEIGHT);
		v.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		stage.setViewport(v);
		stage.getCamera().position.x = GameConstants.SCREENWIDTH / 2;
		stage.getCamera().position.y = GameConstants.SCREENHEIGHT / 2;
		engine = new Engine();
		
		OrthographicCamera cam = (OrthographicCamera) stage.getCamera();

		SpriteDrawable draw = new SpriteDrawable(
				new Sprite(new Texture(Gdx.files.getFileHandle(Resources.RESTART, FileType.Internal))));

		ButtonStyle buttonS = new ButtonStyle(draw, draw, draw);

		final Button button = new Button(buttonS);
		button.setWidth(30);
		button.setHeight(10);
		button.setPosition(GameConstants.SCREENWIDTH - 30, 5);

		button.addListener(new ChangeListener() {

			@Override
			public void changed(ChangeEvent event, Actor actor) {
			
					main.setScreen(new GameScreen(name, main));

			}
		});
		stage.addActor(button);

		engine.addSystem(new StarSystem(cam, null));
		engine.addSystem(new MovementSystem(null));
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void render(float delta) {

		Gdx.gl.glClearColor(0f, 0f, 0f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
		engine.update(Gdx.graphics.getDeltaTime());
		stage.draw();
		stage.getBatch().begin();
		for (DrawAbleComponent tc : getDrawables()) {
			tc.draw((SpriteBatch) stage.getBatch());
		}
		stage.getBatch().end();

		// if (Gdx.input.isTouched())
		// main.setScreen(new GameScreen());
	}



	private List<DrawAbleComponent> getDrawables() {
		drawables = Family.all(TransformComponent.class).one(TextureComponent.class, ParticleComponent.class).get();
		ImmutableArray<Entity> drawAblesArray = engine.getEntitiesFor(drawables);
		List<DrawAbleComponent> drawablesList = new ArrayList<DrawAbleComponent>();
		for (Entity entity : drawAblesArray) {
			if (ComponentMapper.getFor(TextureComponent.class).get(entity) != null)
				drawablesList.add(ComponentMapper.getFor(TextureComponent.class).get(entity));
			else
				drawablesList.add(ComponentMapper.getFor(ParticleComponent.class).get(entity));

		}

		Collections.sort(drawablesList);
		return drawablesList;
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
