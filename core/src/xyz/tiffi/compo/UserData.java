package xyz.tiffi.compo;

import com.badlogic.ashley.core.Entity;

public class UserData {

	private ShapeType type;
	private Entity entity;

	public UserData(ShapeType type, Entity entity) {
		super();
		this.type = type;
		this.entity = entity;
	}

	public ShapeType getType() {
		return type;
	}

	public void setType(ShapeType type) {
		this.type = type;
	}

	public Entity getEntity() {
		return entity;
	}

	public void setEntity(Entity entity) {
		this.entity = entity;
	}

}
