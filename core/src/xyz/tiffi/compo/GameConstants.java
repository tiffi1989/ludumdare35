package xyz.tiffi.compo;

import com.badlogic.gdx.Gdx;

public class GameConstants {
	
	public static final int SCREENWIDTH = (int) (90 * Gdx.graphics.getWidth() / (float)Gdx.graphics.getHeight())  ;
	public static final int SCREENHEIGHT = 90;
	public static final float PLAYERSCALE = 0.05f;
	public static final float STARSCALE = .3f;
	public static final float SHOOTSCALE = .02f ;
	public static final float ENEMYSCALE = .03f ;
	public static final float MINERALESCALE = .01f ;
	public static final float PARTICLESCALE = .2f ;
	public static final int PLAYERSPEED = 80;
	public static final int STARSPEED = -30;
	public static final int SHOOTSPEED = 60;
	public static final int ENEMYSPEED = -50;

}
