package xyz.tiffi.compo;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;

import xyz.tiffi.compo.Components.BodyComponent;
import xyz.tiffi.compo.Components.ParticleComponent;
import xyz.tiffi.compo.Components.TextureComponent;
import xyz.tiffi.compo.Components.TransformComponent;
import xyz.tiffi.compo.screens.GameScreen;
import xyz.tiffi.compo.systems.DestroyBodys;
import xyz.tiffi.compo.systems.PlayerSystem;

public class GameCollision implements ContactListener {

	DestroyBodys destroyer;
	GameScreen game;
	World world;

	public GameCollision(DestroyBodys destroyer, GameScreen game, World world) {
		this.destroyer = destroyer;
		this.game = game;
		this.world = world;
	}

	@Override
	public void beginContact(Contact contact) {
		Body a = contact.getFixtureA().getBody();
		Body b = contact.getFixtureB().getBody();
		UserData useA = (UserData) a.getUserData();
		UserData useB = (UserData) b.getUserData();

		if ((useA.getType() == ShapeType.PLAYER || useB.getType() == ShapeType.PLAYER)) {
			UserData usePlayer;
			UserData useOther;
			if (useA.getType() == ShapeType.PLAYER) {
				usePlayer = useA;
				useOther = useB;
			} else {
				usePlayer = useB;
				useOther = useA;
			}
			
			if(useOther.getType() == ShapeType.MINERAL || useOther.getType() == ShapeType.POINT){
				if(useOther.getType() == ShapeType.MINERAL)
					destroyer.getEngine().getSystem(PlayerSystem.class).addMineral();
				else
					game.score += 50 * game.level;
				
				if (useA.getType() == ShapeType.PLAYER) {
					destroyer.addBody(b);
				} else {
					destroyer.addBody(a);
				}
				
				destroyer.getEngine().removeEntity(useOther.getEntity());
				return;
			}
			
			if (useOther.getType() == ShapeType.CIRCLE || useOther.getType() == ShapeType.SQUARE
					|| useOther.getType() == ShapeType.TRIANGLE)
				return;

			if (useA.getType() == ShapeType.PLAYER) {
				destroyer.addBody(b);
				createEx(b);
			} else {
				destroyer.addBody(a);
				createEx(a);
			}

			destroyer.getEngine().getSystem(PlayerSystem.class).removeMineral();

		}

		if (a.getUserData() instanceof UserData && b.getUserData() instanceof UserData)
			if (canBeKilled(((UserData) a.getUserData()).getType(), ((UserData) b.getUserData()).getType())) {
				destroyer.addBody(b);
				destroyer.addBody(a);
				
				int points = 0;
				switch (((UserData) a.getUserData()).getType()) {
				case CIRCLE:
				case CIRCLEENEMY:
					points = 5;
					break;
				case SQUARE:
				case SQUAREENEMY:
					points = 10;
					break;
				case TRIANGLE:
				case TRIANGLEENEMY:
					points = 20;
					break;
				default:
					break;
				}

				createEx(b);
				
				System.out.println(world);
				
				if(Math.random() <= .25f)
					destroyer.createSth(new Vector2(b.getPosition()));;
				
				game.score += points * game.level;
			}
	}

	
	private void createEx(Body b) {
		Entity ex = new Entity();
		ex.add(new TransformComponent(b.getPosition().x, b.getPosition().y));
		ex.add(new ParticleComponent(Resources.EXPLOSION, TextureLayers.UI, ex, destroyer.getEngine()));
		destroyer.getEngine().addEntity(ex);
	}
	

	@Override
	public void endContact(Contact contact) {
		// TODO Auto-generated method stub

	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		// TODO Auto-generated method stub

	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		// TODO Auto-generated method stub

	}

	public boolean canBeKilled(ShapeType a, ShapeType b) {

		if (a.equals(ShapeType.CIRCLE) && b.equals(ShapeType.CIRCLEENEMY))
			return true;
		if (b.equals(ShapeType.CIRCLE) && a.equals(ShapeType.CIRCLEENEMY))
			return true;
		if (a.equals(ShapeType.SQUARE) && b.equals(ShapeType.SQUAREENEMY))
			return true;
		if (b.equals(ShapeType.SQUARE) && a.equals(ShapeType.SQUAREENEMY))
			return true;
		if (a.equals(ShapeType.TRIANGLE) && b.equals(ShapeType.TRIANGLEENEMY))
			return true;
		if (b.equals(ShapeType.TRIANGLE) && a.equals(ShapeType.TRIANGLEENEMY))
			return true;

		return false;
	}

}
