package xyz.tiffi.compo.Components;

import com.badlogic.ashley.core.Component;

public class TransformComponent implements Component {

	public float posX;
	public float posY;
	public float velX;
	public float velY;

	public TransformComponent(float posX, float posY) {
		super();
		this.posX = posX;
		this.posY = posY;
	}

	public TransformComponent(float posX, float posY, float velX, float velY) {
		super();
		this.posX = posX;
		this.posY = posY;
		this.velX = velX;
		this.velY = velY;
	}

	public float getPosX() {
		return posX;
	}

	public void setPosX(float posX) {
		this.posX = posX;
	}

	public float getPosY() {
		return posY;
	}

	public void setPosY(float posY) {
		this.posY = posY;
	}

	public float getVelX() {
		return velX;
	}

	public void setVelX(float velX) {
		this.velX = velX;
	}

	public float getVelY() {
		return velY;
	}

	public void setVelY(float velY) {
		this.velY = velY;
	}

}
