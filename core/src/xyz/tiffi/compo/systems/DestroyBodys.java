package xyz.tiffi.compo.systems;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

import xyz.tiffi.compo.GameConstants;
import xyz.tiffi.compo.Resources;
import xyz.tiffi.compo.ShapeType;
import xyz.tiffi.compo.TextureLayers;
import xyz.tiffi.compo.UserData;
import xyz.tiffi.compo.Components.BodyComponent;
import xyz.tiffi.compo.Components.TextureComponent;
import xyz.tiffi.compo.Components.TransformComponent;

public class DestroyBodys extends EntitySystem {

	Set<Body> bodies = new HashSet<Body>();
	World world;
	boolean create;
	Vector2 tmp;
	int counter;

	public DestroyBodys(World world) {
		this.world = world;
	}

	@Override
	public void update(float deltaTime) {
		if (create) {
			counter++;
			if(counter > 20){
				create = false;
				if (Math.random() <= .3f)
					createLife(tmp);
				else
					createPoints(tmp);
				
				counter = 0;
			}
	
		}
		for (Body body : bodies) {
			getEngine().removeEntity(((UserData) body.getUserData()).getEntity());

			world.destroyBody(body);
		}

		bodies.clear();

	}

	public void addBody(Body body) {
		bodies.add(body);
	}

	public void createSth(Vector2 vec) {
		create = true;
		tmp = vec.cpy();

	}

	private void createPoints(Vector2 b) {
		Entity ex = new Entity();
		ex.add(new TransformComponent(b.x, b.y, GameConstants.SHOOTSPEED * -1, 0));
		ex.add(new TextureComponent(Resources.POINTSGREEN, TextureLayers.PLAYER, ex, 0.02f,
				(float) Math.random() * 360));
		Shape shape = new CircleShape();
		shape.setRadius(3);
		ex.add(new BodyComponent(world, BodyType.DynamicBody, b.x, b.y, shape, 0, 0, 0, new Vector2(-15, 0),
				new UserData(ShapeType.POINT, ex)));
		getEngine().addEntity(ex);
	}

	private void createLife(Vector2 b) {
		Entity ex = new Entity();
		ex.add(new TransformComponent(b.x, b.y, GameConstants.SHOOTSPEED * -1, 0));
		ex.add(new TextureComponent(Resources.MINERALSBLUE, TextureLayers.PLAYER, ex, 0.02f,
				(float) Math.random() * 360));
		Shape shape = new CircleShape();
		shape.setRadius(3);
		ex.add(new BodyComponent(world, BodyType.DynamicBody, b.x, b.y, shape, 0, 0, 0, new Vector2(-15, 0),
				new UserData(ShapeType.MINERAL, ex)));
		getEngine().addEntity(ex);
	}

}
