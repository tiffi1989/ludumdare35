package xyz.tiffi.compo.systems;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.graphics.Camera;

import xyz.tiffi.compo.GameConstants;
import xyz.tiffi.compo.TextureLayers;
import xyz.tiffi.compo.Components.TextureComponent;
import xyz.tiffi.compo.Components.TransformComponent;
import xyz.tiffi.compo.screens.GameScreen;

public class StarSystem extends EntitySystem {

	private float createTime, deleteTime;
	private List<Entity> stars;
	private Camera camera;
	private int layerCounter;
	boolean firstTime;
	GameScreen game;
	private int level = 1;

	public StarSystem(Camera camera, GameScreen game) {
		this.camera = camera;
		this.game = game;
		stars = new ArrayList<Entity>();

	}

	@Override
	public void update(float deltaTime) {
		if(game != null)
			level = game.level;
		if (!firstTime) {
			for (int i = 0; i < 80; i++) {
				createNewStarSomewhere();
			}
			firstTime = true;
		}
		createTime += deltaTime;
		deleteTime += deltaTime;
		if (createTime >= .2f / (float)level ) {
			createNewStar();
			createTime = 0;
		}
		if (stars.size() > 0 && deleteTime > 5)
			deleteStars();
		super.update(deltaTime);

	}

	private void deleteStars() {
		deleteTime = 0;
		ArrayList<Entity> starsToDelet = new ArrayList<Entity>();
		for (Entity entity : stars) {
			if (ComponentMapper.getFor(TransformComponent.class).get(entity).posX < -10)
				starsToDelet.add(entity);
		}

		for (Entity entity : starsToDelet) {
			stars.remove(entity);
			getEngine().removeEntity(entity);
		}

	}

	private void createNewStar() {
		if (layerCounter >= 100)
			layerCounter = 0;
		Entity entity = new Entity();
		entity.add(new TransformComponent(camera.viewportWidth + 10, (float) (Math.random() * camera.viewportHeight),
				(float) (Math.random() * GameConstants.STARSPEED * level*2), 0));
		entity.add(new TextureComponent("star.png", TextureLayers.STARS + ++layerCounter, entity,
				(float) (Math.random() * GameConstants.STARSCALE) + .05f, game));
		getEngine().addEntity(entity);
		stars.add(entity);
	}

	private void createNewStarSomewhere() {
		if (layerCounter >= 100)
			layerCounter = 0;
		Entity entity = new Entity();
		entity.add(new TransformComponent((float) (Math.random() * (camera.viewportWidth + 10)),
				(float) (Math.random() * camera.viewportHeight), (float) (Math.random() * GameConstants.STARSPEED), 0));
		entity.add(new TextureComponent("star.png", TextureLayers.STARS + ++layerCounter, entity,
				(float) (Math.random() * GameConstants.STARSCALE) + .05f));
		getEngine().addEntity(entity);
		stars.add(entity);
	}

}
