package xyz.tiffi.compo;

public class Resources {
	
	public static final String CIRCLE = "circle.png";
	public static final String LINE = "line.png";
	public static final String TRIFORCE = "triforce.png";
	public static final String SQUARE = "square.png";
	public static final String CIRCLEENEMY = "CircleEnemy.png";
	public static final String TRIANGLEENEMY = "triangleEnemy.png";
	public static final String SQUAREENEMY = "squareEnemy.png";
	public static final String FONT = "font.fnt";
	public static final String MINERALSBLUE = "minerals.png";
	public static final String POINTSGREEN = "pointsgreen.png";
	public static final String EXPLOSION = "explosion.p";
	public static final String START = "start.png";
	public static final String RESTART = "restart.png";

}
