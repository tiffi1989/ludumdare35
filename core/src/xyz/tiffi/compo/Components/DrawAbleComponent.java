package xyz.tiffi.compo.Components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public interface DrawAbleComponent extends Component, Comparable<DrawAbleComponent>{
	
	public void draw(SpriteBatch batch);
	
	public int getPriority();

}
