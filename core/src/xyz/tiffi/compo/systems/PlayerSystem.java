package xyz.tiffi.compo.systems;

import java.util.ArrayList;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

import xyz.tiffi.compo.Input;
import xyz.tiffi.compo.Resources;
import xyz.tiffi.compo.GameConstants;
import xyz.tiffi.compo.TextureLayers;
import xyz.tiffi.compo.UserData;
import xyz.tiffi.compo.ShapeType;
import xyz.tiffi.compo.Components.BodyComponent;
import xyz.tiffi.compo.Components.TextureComponent;
import xyz.tiffi.compo.Components.TransformComponent;
import xyz.tiffi.compo.screens.GameOverScreen;
import xyz.tiffi.compo.screens.GameScreen;

public class PlayerSystem extends EntitySystem {

	private Entity player;
	private TransformComponent tc;
	private TextureComponent texture;
	private boolean firstTime = true;
	private Camera camera;
	private World world;
	private ExtendViewport viewport;
	private ArrayList<Entity> minerals = new ArrayList<Entity>();
	private GameScreen game;
	private BodyComponent bc;
	private long timeLastShot;;

	public PlayerSystem(Camera camera, World world, ExtendViewport viewport, GameScreen game) {
		this.camera = camera;
		this.world = world;
		this.viewport = viewport;
		this.game = game;
		player = new Entity();
		tc = new TransformComponent(10, 10);
		player.add(tc);
		Shape shape = new CircleShape();
		shape.setRadius(200 * GameConstants.PLAYERSCALE);
		bc = new BodyComponent(world, BodyType.DynamicBody, tc.posX, tc.posY, shape, 1, 0, 0, new Vector2(0, 0),
				new UserData(ShapeType.PLAYER, player));
		texture = new TextureComponent("ship.png", TextureLayers.PLAYER, player, GameConstants.PLAYERSCALE);
		player.add(texture);

		player.add(bc);

		((Input) Gdx.input.getInputProcessor()).setPlayer(player);

	}

	@Override
	public void update(float deltaTime) {
		if (firstTime) {
			getEngine().addEntity(player);
			((Input) Gdx.input.getInputProcessor()).setEngine(getEngine());
			addMineral();
			addMineral();
			addMineral();
			firstTime = false;

		}
		int controlFinger = ((Input) Gdx.input.getInputProcessor()).controlFinger;
		if (controlFinger == -1) {
			bc.getBody().setLinearVelocity(0, 0);
			return;
		}

		float mousePos = (viewport.unproject(new Vector2(0, Gdx.input.getY(controlFinger))).y);
		if ((bc.getBody().getPosition().y >= mousePos - 1 && bc.getBody().getPosition().y <= mousePos + 1))
			bc.getBody().setLinearVelocity(0, 0);
		else if (bc.getBody().getPosition().y >= mousePos) {
			bc.getBody().setLinearVelocity(0, GameConstants.PLAYERSPEED * -1f);
		} else
			bc.getBody().setLinearVelocity(0, GameConstants.PLAYERSPEED);
		
		super.update(deltaTime);
	}

	public void shoot(String resource, ShapeType userData) {
		if(System.currentTimeMillis() - timeLastShot < 500)
			return;
		Entity shot = new Entity();
		shot.add(new TransformComponent(50, bc.getBody().getPosition().y, GameConstants.SHOOTSPEED, 0));
		shot.add(new TextureComponent(resource, TextureLayers.SHOTS, shot, GameConstants.SHOOTSCALE,
				(float) Math.random() * 360));
		CircleShape shape = new CircleShape();
		shape.setRadius(200 * GameConstants.SHOOTSCALE);
		shot.add(new BodyComponent(world, BodyType.KinematicBody, tc.posX, bc.getBody().getPosition().y, shape, 0, 0, 0,
				new Vector2(GameConstants.SHOOTSPEED, 0), new UserData(userData, shot)));

		getEngine().addEntity(shot);
	}

	public void addMineral() {
		if (minerals.size() > 5)
			game.score += 50;
		Entity entity = new Entity();
		entity.add(new TransformComponent(5 + (5 * minerals.size()), GameConstants.SCREENHEIGHT - 5));
		entity.add(new TextureComponent(Resources.MINERALSBLUE, TextureLayers.UI, entity, GameConstants.MINERALESCALE));
		getEngine().addEntity(entity);
		minerals.add(entity);
	}

	public void removeMineral() {

		Entity tmp = minerals.get(minerals.size() - 1);
		minerals.remove(minerals.size() - 1);
		getEngine().removeEntity(tmp);
		if (minerals.size() == 0)
			game.main.setScreen(new GameOverScreen(game.main, game.playerName, game.score));
	}
}
