package xyz.tiffi.compo.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;

import xyz.tiffi.compo.GameConstants;
import xyz.tiffi.compo.Resources;
import xyz.tiffi.compo.TextureLayers;
import xyz.tiffi.compo.UserData;
import xyz.tiffi.compo.ShapeType;
import xyz.tiffi.compo.Components.BodyComponent;
import xyz.tiffi.compo.Components.TextureComponent;
import xyz.tiffi.compo.Components.TransformComponent;
import xyz.tiffi.compo.screens.GameScreen;

public class EnemySystem extends EntitySystem {

	private GameScreen game;
	private World world;
	private Camera camera;
	private boolean circleSpawnStart = true;
	private float circleTimer;

	public EnemySystem(GameScreen game, World world, Camera camera) {
		this.game = game;
		this.world = world;
		this.camera = camera;
	}

	@Override
	public void update(float deltaTime) {
		circleTimer += deltaTime;		
		

		float timer = 4f/(float)game.level;
		if(timer <= .2f )
			timer = .2f;
		if (circleSpawnStart == true && circleTimer > timer) {
			spawnRandomEnemy();
			circleTimer = 0;
		}

		// TODO Auto-generated method stub
		super.update(deltaTime);
	}

	private void spawnRandomEnemy() {
		int enemy = (int) (Math.random() * 3);
		switch (enemy) {
		case 0:
			spawnEnemy(ShapeType.CIRCLEENEMY, Resources.CIRCLEENEMY);

			break;
		case 1:
			spawnEnemy(ShapeType.TRIANGLEENEMY, Resources.TRIANGLEENEMY);

			break;
		case 2:
			spawnEnemy(ShapeType.SQUAREENEMY, Resources.SQUAREENEMY);

			break;

		default:
			break;
		}
	}

	private void spawnEnemy(ShapeType userData, String resource) {
		Entity entity = new Entity();
		entity.add(new TransformComponent(0, 0));
		Shape shape = new CircleShape();
		shape.setRadius(200 * GameConstants.ENEMYSCALE);
		entity.add(new BodyComponent(world, BodyType.DynamicBody, camera.viewportWidth + 10,
				5+(float) (Math.random() * (camera.viewportHeight-10)), shape, 0, 0, 0,
				new Vector2(GameConstants.ENEMYSPEED, 0), new UserData(userData, entity)));
		entity.add(new TextureComponent(resource, TextureLayers.ENEMYS, entity, GameConstants.ENEMYSCALE, 0, true));
		getEngine().addEntity(entity);

	}

}
